% From p. 168 of Pedro Gonnet's thesis.

function y = f7(x)
    y = x.^(-0.5);
end
