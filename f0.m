% From
% https://www.gnu.org/software/octave/doc/v4.0.0/Functions-of-One-Variable.html

function y = f0(x)
    y = x .* sin(1./x) .* sqrt(abs(1 - x));
end
