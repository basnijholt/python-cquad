% From p. 168 of Pedro Gonnet's thesis.

function y = f21(x)
    y = 0;
    for i=1:3
        y += 1 ./ cosh(20^i .* (x - 2 .* i ./ 10));
    end
end
