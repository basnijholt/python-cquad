% From p. 167 of Pedro Gonnet's thesis.

function y = f63(x)
    y = abs(x - 0.987654321).^-0.45;
end
