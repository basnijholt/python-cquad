from functools import reduce
import operator
from numbers import Integral
import numpy as np

def _translate_slice(sl):
    """Convert a literal matlab slice (represented as a Python slice
       object) into a Python slice that corresponds to the intended
       indices.
    """
    a, b, c = sl.start, sl.stop, sl.step
    if a is None and b is None and c is None:
        return slice(None)
    elif c is None:
        return slice(a, None if b is None else b + 1)
    else:
        return slice(a, c + 1, b)


class _MatlabRanger:
    """A helper class used to construct ranges with a notation that is
       similar to matlab.  The instances hold no data.
    """
    def __getitem__(self, key):
        sl = _translate_slice(key)
        return range(*sl.indices(sl.stop))

mlrange = _MatlabRanger()


def _translate_index(index):
    """Convert a 1-based matlab general index into a 0-based Python index."""
    if isinstance(index, slice):
        index = _translate_slice(index)
        startstop = []
        for i in [index.start, index.stop]:
            if i is not None:
                i -= 1
            startstop.append(i)
        return slice(*startstop, index.step)
    else:
        return np.asarray(index) - 1


class Array(np.ndarray):
    """An array that offers matlab-like indexing, but otherwise behaves as
    a normal numpy.ndarray.  A view as a ndarray can be obtained
    through the 'np' property.
    """
    def __new__(cls, src):
        arr = np.asarray(src)
        shape = arr.shape
        missing_dims = 2 - len(shape)
        if missing_dims > 0:
            arr = arr.reshape((1,) * missing_dims + shape)
        return arr.view(cls)


    def _key(self, key):
        # Take care of matlab's 1-based indexing.
        if isinstance(key, tuple):
            key = tuple(map(_translate_index, key))
        else:
            key = (_translate_index(key),)
        lkey = len(key)

        # Special case of key being a single full slice:
        # flatten the array into a column vector.
        if (lkey == 1 and isinstance(key[0], slice)
            and key[0].start is key[0].stop is key[0].step is None):
            shape = self.shape[::-1]
            # The following creates tuples of arrays like
            # [[0], [1], [0], [1]], [[0], [0], [1], [1]] that flatten an array
            # in F-order into a column vector.
            return tuple(np.broadcast_to(a, shape).reshape((-1,1))
                         for a in reversed(np.ix_(*(range(r) for r in shape))))

        # Matlab's indexing works like 'numpy.ix_'.  Convert the key
        # into one that can be fed into it.
        shape = self.shape
        rshape = (*shape[:lkey - 1], reduce(operator.mul, shape[lkey - 1:], 1))
        ix_key = []
        for k, s in zip(key, rshape):
            if isinstance(k, Integral):
                ix_key.append([k])
            elif isinstance(k, slice):
                ix_key.append(np.arange(*k.indices(s)))
            else:
                ix_key.append(np.transpose(k).flat)
        key = np.ix_(*ix_key)

        # Allow the last index of the key to also address any
        # remaining dimensions of the array.
        r = range(lkey, self.ndim)
        if r:
            key = list(key)
            for d in r:
                s = shape[d - 1]
                x = key.pop()
                key.append(x % s)
                key.append(x // s)
            key = tuple(key)

        if len(shape) == 2 and shape[1] == 1:
            # Special case: if the original array is a column vector,
            # output also a column vector.
            key = tuple(np.reshape(k, (-1, 1)) for k in key)

        return key


    def __getitem__(self, key):
        # Use ndarray's indexing to get the desired submatrix.
        array = super().__getitem__(self._key(key))

        # Make sure that ndim >= 2, and that there are no "empty"
        # trailing dimensions.
        shape = array.shape
        if len(shape) < 2:
            array.shape = (1, -1)
        else:
            for i in reversed(range(len(shape))):
                if i == 1 or shape[i] != 1:
                    break
            array.shape = shape[:i + 1]

        if array.shape == (1, 1):
            return array.item()

        return array


    def __setitem__(self, key, value):
        # Use ndarray's indexing to get the desired submatrix.
        super().__setitem__(self._key(key), value)


    def __repr__(self):
        return self.__class__.__name__ + repr(np.asarray(self))[5:]

    def __str__(self):
        return str(np.asarray(self))

    def __iter__(self):
        return self.np.reshape((-1,), order='F').__iter__()

    @property
    def np(self):
        return self.view(type=np.ndarray)


def test():
    _ = mlrange

    assert tuple(_[42:42]) == (42,)
    assert tuple(_[2:4]) == (2, 3, 4)
    assert tuple(_[1:2:10]) == (1, 3, 5, 7, 9)

    assert Array([1, 2, 3]).shape == (1, 3)

    a = Array(_[1:120]).reshape((5, 4, 3, 2), order='F')
    assert a[[1,5],[3,4]].tolist() == [[11, 16], [15, 20]]

    assert a[[1,2,3,15]].tolist() == [[1, 2, 3, 15]]
    assert a[Array([1,2,3,15])].tolist() == [[1, 2, 3, 15]]
    assert a[Array([[1,3],[2,15]])].tolist() == [[1, 2, 3, 15]]

    assert a[3, 3] == a[3, 3, 1] == a.np[2, 2, 0, 0]
    assert a[23:23] == 23
    assert a[1:2, 3:4, 5].tolist() == [[91, 96], [92, 97]]
    assert a[2, 2:3, 2, :][:, :, 1, 2].tolist() == [[87, 92]]

    assert a[:].shape == (120, 1)  # a(:) is a special case.
    assert a[1:].shape == (1, 120) # (a:end) is different.
    assert a[1:120].shape == (1, 120)
    assert a[:, :].shape == (5, 24)
    assert a[:, :, :].shape == (5, 4, 6)
    assert a[:, :, :, :].shape == a.shape

    assert a[-5] == 115
    assert a[[0, -3, -10]].tolist() == [[120, 117, 110]]
    assert a[-3:].tolist() == [[117, 118, 119, 120]]

    a = Array(_[0:32])
    assert a.shape == (1, 33)
    assert a[3:].shape == (1, 31)
    assert a.T.shape == (33, 1)
    assert a.T[3:].shape == (31, 1) # This is a special case.

    a = Array(np.zeros((3, 3, 3), dtype=int))
    a[2, :] = 1
    a[:, 2, :] = 2
    a[:, :, 2] = 3
    a[17] = 4
    a[[1, 5, 11, 13]] = 5
    b = a[:,:,2]
    assert b.tolist() == [[3, 5, 3], [5, 3, 4], [3, 3, 3]]

    assert list(iter(b)) == [3, 5, 3, 5, 3, 3, 3, 4, 3]


if __name__ == '__main__':
    test()
