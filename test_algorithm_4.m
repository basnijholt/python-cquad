%format hex

[int, err, nr_points] = algorithm_4 (@f0, 0, 3, 1e-5);
assert(int == hex2num('3fffb6084c1dabf4'))
assert(err == hex2num('3ef46042cb969374'))
assert(nr_points == 1419)

[int, err, nr_points] = algorithm_4 (@f7, 0, 1, 1e-6);
assert(int == hex2num('3fffffffd9fa6513'))
assert(err == hex2num('3ebd8955755be30c'))
assert(nr_points == 709)

[int, err, nr_points] = algorithm_4 (@f24, 0, 3, 1e-3);
assert(int == hex2num('4031aa1505ba7b41'))
assert(err == hex2num('3f9202232bd03a6a'))
assert(nr_points == 4515)

[int, err, nr_points] = algorithm_4 (@f21, 0, 1, 1e-3);
assert(int == hex2num('3fc4e088c36827c1'))
assert(err == hex2num('3f247d00177a3f07'))
assert(nr_points == 203)

[int, err, nr_points] = algorithm_4 (@f63, 0, 1, 1e-10);
assert(int == hex2num('3fff7ccfd769d160'))
assert(err == hex2num('3e28f421b487f15a'))
assert(nr_points == 2715)

[int, err, nr_points] = algorithm_4 (@fdiv, 0, 1, 1e-6);
assert(int == hex2num('7ff0000000000000'))
assert(err == hex2num('4073b48aeb356df5'))
assert(nr_points == 457)
